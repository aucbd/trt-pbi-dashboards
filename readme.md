# Troldtekt Power BI Dashboards


__Dashboard__ are a tool to help visualize the data. The world generates large amounts
of data and one way of making sense out of them is visualization.

The dashboards designed for [Troldtek](https://www.troldtekt.dk/?gclid=CjwKCAiA44LzBRB-EiwA-jJipMkKiqah1ReSqE3imuOW7GBRYWWPJPQ2qDgkp-swuBCagRZmpnDiRxoCKpMQAvD_BwE) enable depiction of defects,
 downtimes and similar measures which will help to turn data into information that can be used
for decision making, defect rate control or even optimization of related processes


# What dashboards are there?


The preview of the dashboards: 

![alt text](https://bitbucket.org/aucbd/trt-pbi-dashboards/raw/df636e7a456d0fe6848dc15cf34ed8a7eca4707e/page1.PNG "Page 1")

![alt text](https://bitbucket.org/aucbd/trt-pbi-dashboards/raw/df636e7a456d0fe6848dc15cf34ed8a7eca4707e/page%202.PNG "Page 2")

![alt text](https://bitbucket.org/aucbd/trt-pbi-dashboards/raw/df636e7a456d0fe6848dc15cf34ed8a7eca4707e/page%203.PNG "Page 3")



The link for the documentation can be found [here](https://bitbucket.org/aucbd/trt-pbi-dashboards/src/master/copy%20of%20Troldtekt%20digital%20twin%20showcase%20(1).pptx)


# Dependencies 

The visuals currently used have no additional dependencies. This may change with later versions.

# Shift time info

Shifts are divided according to time. The specific times are explained in [documentation](https://bitbucket.org/aucbd/trt-pbi-dashboards/src/master/copy%20of%20Troldtekt%20digital%20twin%20showcase%20(1).pptx)

# KPIs

The KPIs tries to visualize the possible range of number of defects one should expect to occur. The middle line represents the expectation based proportion of each type of defects. The proportion is calclated
for the whole time ranges and therefore compares individual weeks(month or days) with what should be expected given the data we have. The expectation is then n*p (expectation of binomial distribution).
The upper and lower bounds are suppose to represent the interval, in which, with overwhelming probability(more than 99%), the number of defects should lie(the empirical rule).
The modeling assumptions were independence of defects and their identical distribution. If the number of defects lie outside of given interval, there was some other factor that affected the number of defects.

# Version
Created in Power BI Version: 2.81.5831.821 64-bit 